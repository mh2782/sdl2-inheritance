#ifndef GAME_H_
#define GAME_H_
#include <vector>
using std::vector;

#include "SDL2Common.h"
#include "Bullet.h"

class Player;
class NPC;

class Game 
{
private:
    // Declare window and renderer objects
    SDL_Window*	    gameWindow;
    SDL_Renderer*   gameRenderer;

    // Background texture
    SDL_Texture*    backgroundTexture;
    
    //Declare Player
    Player*         player;

    //Declare NPC
    NPC*            npc;

    // Window control 
    bool            quit;
    
    // Keyboard
    const Uint8     *keyStates;

    //Bullets
    vector<Bullet*> bullets;

    // Game loop methods. 
    void processInputs();
    void update(float timeDelta);
    void draw();

public:
    // Constructor 
    Game();
    ~Game();

    // Methods
    void init();
    void runGameLoop();
    void createBullet(Vector2f* position, Vector2f* velocity);
    Player* getPlayer();

    // Public attributes
    static const int WINDOW_WIDTH = 800;
    static const int WINDOW_HEIGHT= 600;
};

#endif