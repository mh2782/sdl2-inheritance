#include "Bullet.h"
#include <iostream>
#include "Animation.h"
#include "Vector2f.h"
#include "TextureUtils.h"
#include "Game.h"

Bullet::Bullet() :Sprite() {
	
	state = TRAVEL;
	speed = 100.0f;

	orientation = 0.0f;
	damage = 10.0f;
	lifetime = 20.0f;
	angleOffset = -90.0f;  //MIGHT NEED TO CHANGE THIS 

	targetRectangle.w = SPRITE_WIDTH;
	targetRectangle.h = SPRITE_HEIGHT;
}

Bullet::~Bullet() {

}

void Bullet::init(SDL_Renderer* renderer, Vector2f* position, Vector2f* direction)
{
	//Set orientation
	this->orientation = calculateOrientation(direction);

	std::cout << orientation << std::endl;

	//path string
	string path("assets/images/Arrow3.png");

	//Call sprite constructor
	Sprite::init(renderer, path, 1, position);

	//Set velocity to forward direction of travel
	velocity->setX(direction->getX());
	velocity->setY(direction->getY());
	velocity->normalise();
	velocity->scale(speed);

	//Setup animation structure
	animations[TRAVEL]->init(2, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);

	for (int i = 0; i < maxAnimations; i++) {
		animations[i]->setMaxFrameTime(0.2f);
	}
}

float Bullet::calculateOrientation(Vector2f* direction) {
	
	float angle = atan2f(direction->getY(), direction->getX());
	angle *= (180.0f / 3.142f); //convert to degrees
	return angle + angleOffset;
}

void Bullet::update(float dt) {
	lifetime -= dt;
	Sprite::update(dt);
}

void Bullet::draw(SDL_Renderer* renderer) {
	//Getcurrent animation based on the state
	Animation* current = this->animations[getCurrentAnimationState()];

	//Use extended render copy
	SDL_RenderCopyEx(renderer, 
		texture, 
		current->getCurrentFrame(), 
		&targetRectangle, 
		orientation, 
		nullptr, 
		SDL_FLIP_NONE);
}

int Bullet::getCurrentAnimationState() {
	return state;
}

int Bullet::getDamage() {
	return damage;
}

bool Bullet::hasExpired() {
	if (lifetime > 0.0f) {
		return false;
	}
	else{
		return true;
	}
}

void Bullet::setGame(Game* game) {
	this->game = game;
}