#ifndef BULLET_H_
#define BULLET_H_

#include "SDL2Common.h"
#include "Sprite.h"

class Vector2f;
class Animation;
class Game;


class Bullet : public Sprite {
	
private:

	//Animation state
	int state;

	//Sprite info
	static const int SPRITE_HEIGHT = 32;
	static const int SPRITE_WIDTH = 32;

	//need game
	Game *game;

	int damage;
	float lifetime;

	//angle in degrees
	float orientation;

	//difference between the world forward and the sprite sheet forward
	float angleOffset;

public:
	Bullet();
	~Bullet();

	//Player animation states
	enum BulletState{TRAVEL};

	void init(SDL_Renderer *renderer, Vector2f* position, Vector2f* direction);
	void update(float timeDeltaInSeconds);
	void draw(SDL_Renderer* renderer);

	void setGame(Game* game);

	int getCurrentAnimationState();

	int getDamage();

	float calculateOrientation(Vector2f* direction);

	bool hasExpired();
};


















#endif